package com.bazito.views


import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.TranslateAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bazito.R
import com.bazito.models.Page1
import com.bazito.services.ApiClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private var xCoOrdinate = 0f
    private var yCoOrdinate = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val imgArrow = findViewById<ImageView>(R.id.img3)

        getData()
        getTouch()
        //getAnimation()
    }

    private fun getData() {
        val call: Call<Page1> = ApiClient.getClient.getUsers()
        call.enqueue(object : Callback<Page1> {
            //private var requestBuilder: RequestBuilder<PictureDrawable>? = null
            override fun onResponse(call: Call<Page1>, response: Response<Page1>) {
                //val img1 = findViewById<ImageView>(R.id.img1)
                val imageUri = "http://app.baazito.com/api/File/Resource/GameImages/circle.svg"
                //val imageUri = "https://upload.wikimedia.org/wikipedia/commons/e/e9/Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png"
                Log.v("Mehdi1", response.body()?.result.toString())
                Log.v("Mehdi2", response.body()?.result?.arrow.toString())
                if (response.isSuccessful()) {
                    Log.v("Mehdi3", response.body().toString())
                    //Picasso.get().load("http://app.baazito.com/"+response.body()?.result?.arrow).into(img1)
                    //Picasso.get().load(imageUri).into(img1);
                    //GlideApp.with(img1).load(imageUri).centerCrop().into(img1)
                    //SvgModule.with(img1).load(imageUri).apply(RequestOptions.centerCropTransform()).into(img1)
                    //requestBuilder = Glide.with(this@MainActivity).`as`(PictureDrawable::class.java).listener(SvgSoftwareLayerSetter())

                } else {
                    Toast.makeText(applicationContext,response.message().toString(),Toast.LENGTH_LONG).show()
                }
            }
            override fun onFailure(call: Call<Page1>?, t: Throwable?) {
                Log.d("Mehdi_Fail", t.toString())
            }
        })
    }

    private fun getTouch() {
        img3.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(view: View, event: MotionEvent): Boolean {
                when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> {
                        xCoOrdinate = view.getX() - event.rawX
                        yCoOrdinate = view.getY() - event.rawY
                    }
                    MotionEvent.ACTION_MOVE -> view.animate().x(event.rawX + xCoOrdinate)
                        .y(event.rawY + yCoOrdinate).setDuration(0).start()
                    else -> return false
                }
                return true
            }
        })
    }

    private fun getAnimation() {
        var xCurrentPos = img3.getLeft().toFloat()
        val yCurrentPos = img3.getTop().toFloat()

        val anim: Animation = TranslateAnimation(xCurrentPos, xCurrentPos + 150, yCurrentPos, yCurrentPos)
        anim.duration = 1000
        anim.fillAfter = true
        anim.isFillEnabled = true
        anim.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(arg0: Animation) {}
            override fun onAnimationRepeat(arg0: Animation) {}
            override fun onAnimationEnd(arg0: Animation) {
                xCurrentPos -= 150
            }
        })
        img3.startAnimation(anim)
    }

}

//Toast.makeText(applicationContext,myCall.toString(),Toast.LENGTH_LONG).show()
//Log.i("Mehdi", myCall.toString())
//Log.w("Mehdi gson => ", Gson().toJson(response))
//System.out.println("++++++++++++++ was here+++++++++++++++++++");

