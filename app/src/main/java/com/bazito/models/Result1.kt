package com.bazito.models

data class Result1(
    val arrow: String,
    val circle: String,
    val correctAnswer: String,
    val line: String
)