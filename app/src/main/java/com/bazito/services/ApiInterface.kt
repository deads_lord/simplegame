package com.bazito.services

import com.bazito.models.Page1
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("home/download-resource")
    fun getUsers(): Call<Page1> //Call<List<Page1>>> is False because its return an array instead of object

}



